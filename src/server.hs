{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

import           Common
import           Data.Maybe (fromMaybe)
import           Data.Proxy (Proxy(..))
import qualified Lucid as L
import           Miso
import           Network.Wai.Handler.Warp (run)
import qualified Network.Wai.Middleware.Gzip as Wai
import           Network.Wai.Middleware.RequestLogger (logStdout)
import           Servant
import           System.Environment (lookupEnv)

main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    putStrLn $ "running on port " ++ show port ++ "..."
    run port $ logStdout $ compress app
    where app = serve (Proxy @ServerApi) server
          compress = Wai.gzip Wai.def { Wai.gzipFiles = Wai.GzipCompress }

type ServerApi
    =    StaticApi
    :<|> AddApi
    :<|> HeroesApi
    :<|> ToServerRoutes ClientRoutes HtmlPage Action

server :: Server ServerApi
server 
    =    serveDirectoryFileServer "static"
    :<|> handleAdd
    :<|> pure heroes
    :<|> (pure $ HtmlPage $ homeRoute initialModel)

handleAdd :: Int -> Int -> Handler Int
handleAdd x y = pure $ x + y

heroes :: [Hero]
heroes = 
    [ Hero "Scooby Doo" "scoobydoo.png"
    , Hero "Sponge Bob" "spongebob.png"
    ]

newtype HtmlPage a = HtmlPage a
    deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
    toHtmlRaw = L.toHtml
    toHtml (HtmlPage x) = L.doctypehtml_ $ do
        L.head_ $ do
            L.meta_ [L.charset_ "utf-8"]
            L.with 
                (L.script_ mempty) 
                [L.src_ "static/all.js", L.async_ mempty, L.defer_ mempty] 
        L.body_ (L.toHtml x)

