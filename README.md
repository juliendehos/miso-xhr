# miso-xhr

<https://miso-xhr.herokuapp.com/>

## description

- isomorphic web app in Haskell, using [Miso](https://github.com/dmjio/miso)
  and [Servant](https://haskell-servant.github.io)

- fetch data using XHR

- type-safe routes & links (client + server)

![](doc/miso-xhr.mp4)


## setup

- install Nix, docker and heroku-cli


## develop

- use a nix-shell for the client:

```
nix-shell -A client
make client
```

- use a nix-shell for the server:

```
nix-shell -A server
make server
```

- test in a browser: <http://localhost:3000>


## deploy (manually)

- build a docker image:

```
nix-build nix/docker.nix && docker load < result
```

- (test docker image):

```
docker run -p 3000:3000 -it miso-xhr
```

- upload docker image to heroku:

```
heroku login
heroku container:login
heroku create miso-xhr
docker tag miso-xhr:latest registry.heroku.com/miso-xhr/web
docker push registry.heroku.com/miso-xhr/web
heroku container:release web --app miso-xhr
```

- (test heroku app):

```
heroku logs
heroku open --app miso-xhr
```

## deploy (using gitlab CI/CD)

- add a HEROKU_API_KEY variable in the Gitlab repo (see account setting in Heroku)

- create a container app in Heroku and add a APP_NAME variable in the Gitlab repo 

- push the master branch for updating the app

Warning: use a docker image containing a pre-built ghc/ghcjs
environment (e.g.
[juliendehos/cicd:misomorphic](https://hub.docker.com/r/juliendehos/cicd/tags),
see [dockerhub](dockerhub)).

