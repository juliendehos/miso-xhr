Build a docker image and upload it to dockerhub, for CI/CD:

```
docker build -t juliendehos/cicd:miso-xhr -f Dockerfile ..
docker login
docker push juliendehos/cicd:miso-xhr
docker logout
```

**Warning: this takes about 2 hours and requires more than 4 GB RAM !**

You have to build & upload a new image, if you change something in [the nixpkgs.nix file](../nix/nixpkgs.nix).

