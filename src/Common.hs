{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import Data.Proxy (Proxy(..))
import GHC.Generics (Generic)
import Miso
import Miso.String (concat, MisoString, ms)
import Prelude hiding (concat)
import Network.HTTP.Types.Method (Method)
import Network.URI (URI)
import Servant.API
import Servant.Client
import Servant.Links

data Hero = Hero
    { heroName :: MisoString
    , heroImage :: MisoString
    } deriving (Eq, Generic, Show)

instance FromJSON Hero
instance ToJSON Hero

newtype Model = Model 
    { heroes_ :: [Hero]
    } deriving (Eq, Generic)

instance ToJSON Model

initialModel :: Model
initialModel = Model []

data Action
    = NoOp
    | SetHeroes [Hero]
    | FetchHeroes
    deriving (Eq, Show)

type ClientRoutes = HomeRoute

type HomeRoute = View Action

homeRoute :: Model -> View Action
homeRoute m = div_ 
    []
    [ h1_ [] [ text "miso-xhr" ]
    , ul_ [] (map fmtHero $ heroes_ m)
    , p_ [] [ a_ [href_ $ ms $ show linkHeroes] [ text "JSON data" ] ] 
    , p_ [] [ a_ [href_ $ ms $ show $ linkAdd 20 22 ] [ text "add 20 22" ] ] 
    , p_ [] [ a_ [href_ "https://gitlab.com/juliendehos/miso-xhr"] [ text "source code" ] ] 
    ]
    where fmtHero h = li_ [] 
            [ text $ heroName h
            , br_ []
            , img_ [ src_ $ concat [ms $ show linkStatic, "/", heroImage h] ]
            ]

type StaticApi = "static" :> Raw 
type AddApi = "add" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Int
type HeroesApi = "heroes" :>  Get '[JSON] [Hero] 
type PublicApi = StaticApi :<|> AddApi :<|> HeroesApi 

fetchStatic :: Method -> ClientM Response
fetchAdd :: Int -> Int -> ClientM Int
fetchHeroes :: ClientM [Hero]
fetchStatic :<|> fetchAdd :<|> fetchHeroes = client (Proxy @PublicApi)

linkStatic :: URI
linkStatic = linkURI $ safeLink (Proxy @PublicApi) (Proxy @StaticApi)

linkAdd :: Int -> Int -> URI
linkAdd x y = linkURI (safeLink (Proxy @PublicApi) (Proxy @AddApi) x y)

linkHeroes :: URI
linkHeroes = linkURI $ safeLink (Proxy @PublicApi) (Proxy @HeroesApi)

